# addresses to gpx



## Getting started

1) Aqcuire Google maps API key for geocoding
2) Rename api_key.json.sample to api_key.json and replace key inside with your API KEY.

## Usage

### If you have bunch of addresses in a text file
cat /path/to/text/file | node.exe app.js > output_file.gpx

### otherwise you can just echo a line in to it
echo "Suolijärvi, Hervanta" | node.exe app.js > output_file.gpx

