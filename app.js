const console = require('node:console');
const fs = require('node:fs');
const Client = require("@googlemaps/google-maps-services-js").Client;

const API_KEY = require('./api_key.json').API_KEY;

async function geocode(address_to_geocode) {

	var location = undefined;
	try {
		const client = new Client({});
		let result = await client
		  .geocode({
			"params": {
			  "address": address_to_geocode,
			  "key": API_KEY
			},
			"timeout": 5000 // milliseconds
		  });
		if (result.data.results.length > 0) {
			location = result.data.results[0].geometry.location;
			console.error("geocode():: hit ", address_to_geocode, location);
		}
		else {
			console.error("geocode():: miss", address_to_geocode, location);
		}
	}
	catch(error) {
		console.error("geocode()::Error", error);
		throw error;
	}
	

	return location;
}

function print_gpx_wpt(address_to_geocode, location) {
	console.log(`<wpt lat="${location.lat}" lon="${location.lng}">`);
	console.log(`	<name>${address_to_geocode}</name>`);
	console.log(`	<sym>City</sym>`);
	console.log(`</wpt>`);
}

function print_gpx_header() {
	console.log(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="Wikipedia"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
<metadata>
	<name>Addresses to GPX inator</name>
	<desc>Viuttu että juilii</desc>
	<author>
		<name>Martti Ahtisaari</name>
	</author>
</metadata>`);
}

async function main() {
	print_gpx_header();
	try {
		const data = fs.readFileSync(0, 'utf8');
		const lines = data.toString().split(/\r?\n/);
		for (let line of lines) {
			if (line.length > 4) {
				let location = await geocode(line);
				// console.log("hihoo", line);
				if (location)
				{
					print_gpx_wpt(line, location);
				}
			}
			else {
				console.error("main()::skipping short line", line);
			}
		}
	}
	catch (err) {
	  console.error(err);
	}

	console.log(`</gpx>`);
}

main();